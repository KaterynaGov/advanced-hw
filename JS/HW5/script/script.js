class Card {
  constructor(title, text, id, data) {
    this.title = title;
    this.text = text;
    this.id = id;
    this.data = data;
  }
}

const headMenu = document.querySelector(".card");

const addNewValues = (oldObj, newObj) => {
  if (oldObj.id === newObj.id) {
    oldObj.name = newObj.name;
    oldObj.email = newObj.email;
  }
};

const addNewPost = (obj) => {
  let newDiv = document.createElement("div");
  newDiv.innerHTML = `
<div data-id ='${obj.data}' class="d-flex p-3 border border-bottom">
    <img src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png" class="rounded-circle"
      height="50" alt="Avatar" loading="lazy" />
    <div class="d-flex w-100 ps-3">
      <div>
        <a href="">
          <h6 class="text-body">
              ${obj.name}
            <span class="small text-muted font-weight-normal">${obj.email}</span>
            <span><i class="fas fa-angle-down float-end"></i></span>
          </h6>
        </a>
        <h5  style="line-height: 1.5">${obj.title}</h5>
        <p style="line-height: 1.2">${obj.text}
        </p>
        <ul class="list-unstyled d-flex justify-content-between mb-0 pe-xl-5">
          <li>
            <i class="far fa-comment"></i>
          </li>
          <li>
            <i class="far fa-share-square"></i>
          </li>
        </ul>
      </div>
    </div>
    <div class="btn-wrap">
        <button type="button" class="btn btn-primary btn-rounded">DELETE</button>
    </div>
</div>`;
  headMenu.after(newDiv);
};

const spinnerRemove = () => {
  let spin = document.querySelector(".spinner-wrap");
  spin.remove();
};

const postCreate = async () => {
  const users = await fetch("https://ajax.test-danit.com/api/json/users"); //"./json/users.json"
  const usersJson = await users.json();

  const posts = await fetch("https://ajax.test-danit.com/api/json/posts"); //"./json/posts.json"
  const postsJson = await posts.json();
  console.log(usersJson);
  console.log(postsJson);
  let postsArr = await postsJson.map(async (post) => {
    const cardCreate = async (el) => {
      let card = new Card(el.title, el.body, el.userId, el.id);
      await usersJson.forEach(async (user) => {
        addNewValues(card, user);
      });
      return card;
    };
    let newPost = await cardCreate(post);
    return newPost;
  });
  await postsArr.forEach((element) => {
    element.then((res) => {
      addNewPost(res);
    });
  });
  spinnerRemove();
};

window.addEventListener("load", postCreate);

let btn = document.querySelector(".btn-primary");

document.addEventListener("click", async (ev) => {
  if (ev.target.innerText === "DELETE") {
    let postId = ev.target.parentElement.parentElement.getAttribute("data-id");
    let response = await fetch(
      `https://ajax.test-danit.com/api/json/posts/${postId}`,
      { method: "DELETE" }
    );
    if (response.ok) {
      ev.target.parentElement.parentElement.remove();
    }
  }
});

const addNewTweet = async (tweetObj) => {
  let res = await fetch(`https://ajax.test-danit.com/api/json/posts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(tweetObj),
  });
  if ((await res.status) === 200) {
    response = await res.json();
    const users = await fetch("https://ajax.test-danit.com/api/json/users"); //"./json/users.json"
    const usersJson = await users.json();
    let card = new Card(
      response.title,
      response.text,
      response.userId,
      response.id
    );
    await usersJson.forEach(async (user) => {
      addNewValues(card, user);
    });
    addNewPost(card);
  }
};

const getFormValue = async (event) => {
  event.preventDefault();
  const title = document.querySelector("#recipient-title");
  const text = document.querySelector("#message-text");
  const userPost = {
    userId: 1,
    title: title.value,
    text: text.value,
  };
  addNewTweet(userPost);
  title.value = null;
  text.value = null;
};

const submitButton = document.querySelector(".btn-submit");

submitButton.addEventListener("click", getFormValue);
