// ## Задание
// 1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта. 
// 2. Создайте геттеры и сеттеры для этих свойств.
// 3. Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// 4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// 5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.


class Employee {
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name (name) {
        return this._name = name;
    }
    get name (){
        return this._name;
    }
    set age (age) {
        return this._age = age;
    }
    get age (){
        return this._age;
    }
    set salary (salary) {
        return this._salary = salary;
    }
    get salary (){
        return this._salary;
    }
}

 class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this._lang = lang;
    } 
    get salary() {
        return super.salary * 3
    }
    set lang (lang) {
        this._lang.push(lang);
        return this._lang;
    }
    get lang (){
        return this._lang;
    }
    
}

const system = new Programmer('Ivan',35,100,['c#','c++','java']);

const applied = new Programmer('Sergio',26,200,['Python']);

const web = new Programmer('Katya',30,300,['html','css','js']);


console.log(system);
console.log(applied);
console.log(web);

// console.log(system.salary);
// console.log(applied.salary);
// web.lang = "scc" ;    
// console.log(web.lang);
