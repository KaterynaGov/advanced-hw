// - Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// - На странице должен находиться `div` с `id="root"`, куда и нужно будет положить этот список (похожая задача была дана в модуле basic). 
// - Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте. 
// - Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];



const createBooks = (arr)=>{

    const container = document.createElement('div');
    container.setAttribute('id', 'root');
    document.body.append(container);

    const creator = (object)=>{
        let newList = document.createElement('ul');
        container.append(newList);
        for (const key in object) {
            newList.insertAdjacentHTML("beforeend", `<li>${key}: ${object[key]}</li>`)
        }
    }

    arr.map((listItem)=>{
        const keyNames = ['author','name','price']
        for (const key in listItem) {
            keyNames.map((name,i)=>{
                if (name === key){  
                    keyNames.splice(i, 1)
                }
            })
        }
        try {
            if(keyNames.length === 0){
                creator(listItem)                              
            }else{
                throw new Error (`В списке отсутствует: ${keyNames.toString()}.`)
            }  
        } catch (error) {
            console.log(error);
        }
    })
    
}
createBooks(books)

