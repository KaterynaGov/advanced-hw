const button = document.querySelector("button");

const addInfoList = (obj)=>{
  const main = document.querySelector('main');
  let list = document.createElement('ul');
  list.innerHTML = `
  <li>Континент: ${obj.continent}</li>
  <li>Країна: ${obj.country}</li>
  <li>Регіон: ${obj.regionName}</li>
  <li>Населений пункт: ${obj.city}</li>
  `
  main.appendChild(list)
}

const searchByIp = async(ip)=>{
  const info = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`);
  const infoJson = await info.json();
  console.log(infoJson);
  await addInfoList(infoJson)
}

button.addEventListener("click", async() => {
  const ip = await fetch(`https://api.ipify.org/?format=json`);
  const json = await ip.json()
  await searchByIp(json.ip)

  });

// У этого кода возникла проблема во время открытия через LiveServer в FireFox : 'Запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://api.ipify.org/?format=json. (Причина: не удалось выполнить запрос CORS). Код состояния: (null).'

// Читала статьи по кросс-доменным запросам, но так и не разобралась толком в чем проблема. Подскажите где почитать или что сделать, чтоб в следующий раз не тупить ^_^ над подобной проблемой.

// Но в Chrome и Edge все ок работает.    
