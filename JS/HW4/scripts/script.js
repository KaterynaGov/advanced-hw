const starWars = () => {
  const filmList = document.createElement("ul");

  document.body.append(filmList);

  const filmography = fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      response.forEach((el) => {
        let newLi = document.createElement("li");
        newLi.setAttribute("id", `film${el.episodeId}`);
        newLi.innerHTML = `<h4 class="film__name">Episode ${el.episodeId} - ${el.name}</h4><p class="film__credits">${el.openingCrawl}</p> <br>`;
        filmList.append(newLi);
        characters(el.episodeId, el.characters);
      });
    });

  const characters = (number, arr) => {
    const charactersList = document.createElement("p");
    const film = document.querySelector(`#film${number} .film__name`);
    film.after(charactersList);
    const newArr = [];
    arr.forEach((el) => {
      fetch(el)
        .then((response) => response.json())
        .then((response) => response.name)
        .then((name) => {
          let role = document.createElement("span");
          role.innerText = `${name} `;
          charactersList.append(role);
        });
    });
  };
};

starWars();
