const burger = () =>{
    document.body.querySelector('.burger-button').addEventListener('click', (e)=>{
        document.querySelector('.burger').classList.toggle('opened')
        e.target.classList.toggle('fa-bars');
        e.target.classList.toggle('fa-times');
    })

}
burger()
